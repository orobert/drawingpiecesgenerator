# DrawingPiecesGenerator

Tool to find pieces from SVG drawing, numerate and organize them to be cut by Cricut tool.

This tool is initially used to build stained glass