﻿using CsPotrace;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Spatial;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DrawingPiecesGenerator
{
    public class Pieces
    {
        [Serializable]
        public class ExtractionResult
        {
            public List<Piece> Pieces = new List<Piece>();
            [NonSerialized]
            [XmlIgnore]
            public Bitmap MasterImage;
        }

        [Serializable]
        public class Piece
        {
            public int No;
            //public ArrayList Shape;
            public Point CenterPoint;

            [NonSerialized]
            [XmlIgnore]
            public Bitmap IsolatedImage;
            public Point IsolatedImagePoint;
            public Point IsolatedImageCenterPoint;

            public ArrayList IsolatedImageShape;

            [NonSerialized]
            [XmlIgnore]
            public int Page;
            [NonSerialized]
            [XmlIgnore]
            public Point PageImagePosition;

            public string ColorLegend;
        }

        public static ExtractionResult generatePiecesAndMaster(string source, string page, string filesPath, Bitmap sourceImage, Dictionary<int, string> colors)
        {
            DateTime srcDate = File.GetLastWriteTime(source);
            DateTime dataDate = DateTime.MinValue;

            string dataPath = Path.Combine(filesPath, "pieces." + page + ".xml");

            if (File.Exists(dataPath))
            {
                dataDate = File.GetLastWriteTime(dataPath);
            }

            if (srcDate == dataDate)
            {
                return loadExtractionResult(dataPath);
            }

            ExtractionResult result = new ExtractionResult();

            int h = sourceImage.Height;
            int w = sourceImage.Width;

            Bitmap baseImage = ImageProcessing.normalizeImage(sourceImage);
            result.MasterImage = new Bitmap(baseImage);

            int no = 1;
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    Color c = baseImage.GetPixel(x, y);
                    if (c.R == 0 && c.G == 0 && c.B == 0)
                        continue;

                    //ArrayList pieceShape;
                    Point isolatedImagePoint;
                    Bitmap isolatedImage;
                    ArrayList isolatedImageShape;
                    extractPiece(x, y, baseImage, out isolatedImagePoint, out isolatedImage, out isolatedImageShape);

                    if (isolatedImage == null)
                        continue;

                    Piece piece = new Piece();
                    piece.No = no;
                    //piece.Shape = pieceShape;
                    piece.IsolatedImage = isolatedImage;
                    piece.IsolatedImagePoint = isolatedImagePoint;
                    piece.IsolatedImageShape = isolatedImageShape;

                    result.Pieces.Add(piece);

                    int pc = 100 - GetPourcentFound(baseImage);

                    Console.WriteLine("Found piece #{0} (Done: {1}%)", no, pc);

                    no++;
                }
            }

            Graphics grMaster = Graphics.FromImage(result.MasterImage);
            SolidBrush blackBrush = new SolidBrush(Color.Black);
            foreach (Piece piece in result.Pieces)
            {
                Point p = getCenterPoint(piece);
                piece.IsolatedImageCenterPoint = p;
                piece.CenterPoint = new Point(p.X + piece.IsolatedImagePoint.X, p.Y + piece.IsolatedImagePoint.Y);

                Color color = sourceImage.GetPixel(piece.CenterPoint.X, piece.CenterPoint.Y);
                int c = color.ToArgb() & 0xffffff;
                piece.ColorLegend = colors[c];
            }

            saveExtractionResult(dataPath, result);
            File.SetLastWriteTime(dataPath, srcDate);
            return result;
        }

        private static int GetPourcentFound(Bitmap image)
        {
            int H = image.Height;
            int W = image.Width;

            bool[,] matrix = Potrace.BitMapToBinary(image, 130);

            int count = 0;
            for (int y = 0; y < H; y++)
            {
                for (int x = 0; x < W; x++)
                {
                    if (matrix[x, y])
                        count++;
                }
            }

            return (int)((float)count / (float)(H * W) * 100);
        }

        private static void saveExtractionResult(string path, ExtractionResult result)
        {
            string dir = Path.GetDirectoryName(path);
            string name = Path.GetFileNameWithoutExtension(path);
            string piecesDir = Path.Combine(dir, name);

            List<Type> extraTypes = new List<Type>();
            extraTypes.Add(typeof(ArrayList));
            extraTypes.Add(typeof(Potrace.Curve));
            extraTypes.Add(typeof(Potrace.Curve[]));

            XmlSerializer xml = new XmlSerializer(typeof(ExtractionResult), extraTypes.ToArray());
            FileStream file = new FileStream(path, FileMode.Create);

            xml.Serialize(file, result);

            file.Flush();
            file.Close();

            if (!Directory.Exists(piecesDir))
            {
                Directory.CreateDirectory(piecesDir);
            }

            foreach(Pieces.Piece piece in result.Pieces)
            {
                piece.IsolatedImage.Save(Path.Combine(piecesDir, piece.No.ToString() + ".png"));
            }

            result.MasterImage.Save(Path.Combine(dir, name + ".master.png"));
        }

        private static ExtractionResult loadExtractionResult(string path)
        {
            string dir = Path.GetDirectoryName(path);
            string name = Path.GetFileNameWithoutExtension(path);
            string piecesDir = Path.Combine(dir, name);

            List<Type> extraTypes = new List<Type>();
            extraTypes.Add(typeof(ArrayList));
            extraTypes.Add(typeof(Potrace.Curve));
            extraTypes.Add(typeof(Potrace.Curve[]));

            XmlSerializer xml = new XmlSerializer(typeof(ExtractionResult), extraTypes.ToArray());
            FileStream file = new FileStream(path, FileMode.Open);

            ExtractionResult result = (ExtractionResult)xml.Deserialize(file);

            file.Close();

            foreach (Pieces.Piece piece in result.Pieces)
            {
                piece.IsolatedImage = (Bitmap)Bitmap.FromFile(Path.Combine(piecesDir, piece.No.ToString() + ".png"));
            }

            result.MasterImage = (Bitmap)Bitmap.FromFile(Path.Combine(dir, name + ".master.png"));

            return result;
        }

        private static void extractPiece(int x, int y, Bitmap baseImage, out Point isolatedImagePoint, out Bitmap isolatedImage, out ArrayList isolatedImageShape)
        {
            Bitmap image = new Bitmap(baseImage);
            Graphics gr = Graphics.FromImage(image);

            ImageProcessing.floodFill(image, x, y, Color.Red);

            isolatedImage = ImageProcessing.isolateImage(image, Color.Red, out isolatedImagePoint);

            ImageProcessing.transfertImage(image, Color.Red, baseImage, Color.Black, new Rectangle(0, 0, baseImage.Width, baseImage.Height), new Point(0, 0));

            if (isolatedImage == null)
            {
                isolatedImageShape = null;
                return;
            }

            ImageProcessing.invert(isolatedImage);

            bool[,] matrix = Potrace.BitMapToBinary(isolatedImage, 130);
            isolatedImageShape = new ArrayList();
            Potrace.potrace_trace(matrix, isolatedImageShape);

            //SvgCreator c = new SvgCreator();
            //c.add(isolatedImageShape, Color.Green);
            //c.save(@"D:\vitrail\test1\drawing\test.svg", isolatedImage.Width, isolatedImage.Height);
        }

        private static Point getCenterPoint(Piece piece)
        {
            int width = piece.IsolatedImage.Width;
            int height = piece.IsolatedImage.Height;
            string coords = "";
            ArrayList path = (ArrayList)piece.IsolatedImageShape[0];

            for (int i = 0; i < path.Count; i++)
            {
                Potrace.Curve[] curves = (Potrace.Curve[])path[i];

                Potrace.Curve curve = curves[0];
                coords += string.Format("{0} {1}", curve.A.x, curve.A.y);

                for (int j = 0; j < curves.Length; j++)
                {
                    curve = curves[j];
                    coords += string.Format(",{0} {1}", curve.B.x, curve.B.y);
                }
            }

            DbGeometry geo = DbGeometry.PolygonFromText("POLYGON((" + coords + "))", 4326);

            DbGeometry rect1 = DbGeometry.PolygonFromText(string.Format("POLYGON(({0} {1},{2} {3},{4} {5},{6} {7},{8} {9}))",
                0, 0,
                width / 2, 0,
                width / 2, height / 2,
                0, height / 2,
                0, 0), 4326);
            DbGeometry rect2 = DbGeometry.PolygonFromText(string.Format("POLYGON(({0} {1},{2} {3},{4} {5},{6} {7},{8} {9}))",
                width / 2, 0,
                width, 0,
                width, height / 2,
                width / 2, height / 2,
                width / 2, 0), 4326);
            DbGeometry rect3 = DbGeometry.PolygonFromText(string.Format("POLYGON(({0} {1},{2} {3},{4} {5},{6} {7},{8} {9}))",
                0, height / 2,
                width / 2, height / 2,
                width / 2, height,
                0, height,
                0, height / 2), 4326);
            DbGeometry rect4 = DbGeometry.PolygonFromText(string.Format("POLYGON(({0} {1},{2} {3},{4} {5},{6} {7},{8} {9}))",
                width / 2, height / 2,
                width, height / 2,
                width, height,
                width / 2, height,
                width / 2, height / 2), 4326);

            DbGeometry zone1 = geo.Intersection(rect1);
            DbGeometry zone2 = geo.Intersection(rect2);
            DbGeometry zone3 = geo.Intersection(rect3);
            DbGeometry zone4 = geo.Intersection(rect4);

            double maxArea = -1;
            DbGeometry usedZone = null;
            bool oneIsEmpty = false;
            for (int i = 0; i < 4; i++)
            {
                DbGeometry zone = null;
                switch (i)
                {
                    case 0: zone = zone1; break;
                    case 1: zone = zone2; break;
                    case 2: zone = zone3; break;
                    case 3: zone = zone4; break;
                }
                if (zone.IsEmpty)
                    oneIsEmpty = true;
                if (zone.Area.HasValue && zone.Area.Value > maxArea)
                {
                    maxArea = zone.Area.Value;
                    usedZone = zone;
                }
            }
            if (!oneIsEmpty)
            {
                usedZone = geo;
            }

            float x = (float)usedZone.Centroid.XCoordinate.Value;
            float y = (float)usedZone.Centroid.YCoordinate.Value;

            return new Point((int)Math.Round(x, 0), (int)Math.Round(y, 0));
        }
    }
}
