﻿using System;
using System.Drawing;
using System.IO;
using System.Text;

namespace DrawingPiecesGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 0)
                return;

            string svgPath = args[0];

            ////////////////////////
            string name = Path.GetFileNameWithoutExtension(svgPath);
            string sourcePath = Path.GetDirectoryName(svgPath);

            string path = Path.Combine(sourcePath, name + "_files");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            string source = Path.Combine(path, "source.svg");
            string drawing = Path.Combine(path, "drawing.svg");
            ////////////////////////


            DateTime svgDate = File.GetLastWriteTime(svgPath);
            DateTime sourceDate = DateTime.MinValue;
            DateTime drawingDate = DateTime.MinValue;

            if (File.Exists(source))
                sourceDate = File.GetLastWriteTime(source);

            bool sourceChanged = false;
            if (svgDate != sourceDate)
            {
                File.Copy(svgPath, source, true);
                File.SetLastWriteTime(source, svgDate);
                sourceDate = svgDate;
                sourceChanged = true;
            }

            if (!(sourceChanged || !File.Exists(drawing)))
            {
                drawingDate = File.GetLastWriteTime(drawing);
            }

            if (drawingDate != sourceDate)
            {
                File.Copy(source, drawing, true);
                SvgProcessing.hidePages(drawing);
                File.SetLastWriteTime(drawing, sourceDate);
            }

            string pageBorderId = "border";

            SvgProcessing.Details svgDetails = SvgProcessing.extractDetails(source);

            Bitmap sourceImage = SvgProcessing.getAsBitmap(drawing, pageBorderId);

            Pieces.ExtractionResult extractionResult = Pieces.generatePiecesAndMaster(source, pageBorderId, path, sourceImage, svgDetails.Colors);

            PiecesOrganize.organize(source, pageBorderId, extractionResult.Pieces);

            ImagesGenerator.generateImages(extractionResult, sourcePath, name);
        }
    }
}
