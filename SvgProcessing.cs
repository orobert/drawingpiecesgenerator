﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DrawingPiecesGenerator
{
    class SvgProcessing
    {
        private enum NodeType
        {
            Legend,
            Page,
        }

        public class Details
        {
            public List<string> Pages = new List<string>();
            public Dictionary<int, string> Colors = new Dictionary<int, string>();
        }

        public static Details extractDetails(string path)
        {
            XmlDocument doc = loadAsXml(path);

            Dictionary<XmlNode, NodeType> list = extractNode(doc);

            Details details = new Details();
            foreach (KeyValuePair<XmlNode, NodeType> item in list)
            {
                switch (item.Value)
                {
                    case NodeType.Page:
                        details.Pages.Add(item.Key.Attributes["id"].InnerText);
                        break;
                    case NodeType.Legend:
                        string style = item.Key.Attributes["style"].InnerText;
                        string[] items = style.Split(';');
                        foreach(string styleItem in items)
                        {
                            if (styleItem.StartsWith("fill:"))
                            {
                                int pos = styleItem.IndexOf("#");
                                string color = styleItem.Substring(pos + 1);
                                string text = item.Key.InnerText;
                                int c = Int32.Parse(color, System.Globalization.NumberStyles.HexNumber);
                                details.Colors.Add(c, text);
                            }
                        }
                        break;
                }
            }
            details.Pages.Sort();

            return details;
        }

        public static XmlDocument loadAsXml(string path)
        {
            string content = File.ReadAllText(path);
            int pos = content.IndexOf("<svg");
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(content.Substring(pos));
            return xmlDoc;
        }

        public static void hidePages(string path)
        {
            XmlDocument doc = loadAsXml(path);
            Dictionary<XmlNode, NodeType> list = extractNode(doc);

            foreach (KeyValuePair<XmlNode, NodeType> item in list)
            {
                if (item.Value == NodeType.Page)
                {
                    string style = item.Key.Attributes["style"].InnerText;
                    List<string> items = new List<string>(style.Split(';'));
                    int usedIndex = -1;
                    for (int i = 0; i < items.Count; i++)
                    {
                        if (items[i].StartsWith("opacity:"))
                        {
                            usedIndex = i;
                            break ;
                        }
                    }

                    /*int value = 0;
                    if (item.Key.Attributes["inkscape:label"].InnerText == "fixed")
                        value = 1;
                    */
                    int value = 1;

                    if(usedIndex == -1)
                    {
                        usedIndex = items.Count;
                        items.Add("");
                    }

                    items[usedIndex] = string.Format("opacity:{0}.0", value);
                    item.Key.Attributes["style"].Value = string.Join(";", items);
                }
            }

            File.WriteAllText(path, doc.InnerXml);
        }

        public static Bitmap getAsBitmap(string path, string page)
        {
            DateTime srcDate = File.GetLastWriteTime(path);
            DateTime bmpDate = DateTime.MinValue;
            string name = Path.GetFileNameWithoutExtension(path);
            string dir = Path.GetDirectoryName(path);
            string bmpPath = Path.Combine(dir, name + "." + page + ".png");

            if (File.Exists(bmpPath))
            {
                bmpDate = File.GetLastWriteTime(bmpPath);
            }

            if (srcDate != bmpDate)
            {
                runProcess(@"C:\Program Files\Inkscape\inkscape.com", string.Format(@"-e ""{0}"" --export-id {1} --export-dpi {2} --export-background white ""{3}""", bmpPath, page, ImagesGenerator.FULL_DPI, path));
                File.SetLastWriteTime(bmpPath, srcDate);
            }

            return (Bitmap)Bitmap.FromFile(bmpPath);
        }

        private static Dictionary<XmlNode, NodeType> extractNode(XmlNode topNode)
        {
            Dictionary<XmlNode, NodeType> list = new Dictionary<XmlNode, NodeType>();
            foreach (XmlNode n in topNode.ChildNodes)
            {
                if (n.Attributes != null)
                {
                    for (int i = 0; i < n.Attributes.Count; i++)
                    {
                        XmlAttribute a = n.Attributes[i];
                        if (a.Name == "id" && a.Value.StartsWith("page"))
                        {
                            list[n] = NodeType.Page;
                        }
                        else if (a.Name == "inkscape:label" && a.Value == "legend")
                        {
                            list[n] = NodeType.Legend;
                        }
                    }
                }

                if (n.ChildNodes.Count > 0)
                {
                    Dictionary<XmlNode, NodeType> subList = extractNode(n);
                    foreach (KeyValuePair<XmlNode, NodeType> item in subList)
                    {
                        list.Add(item.Key, item.Value);
                    }
                }
            }

            return list;
        }

        private static void runProcess(string exe, string arg)
        {
            ProcessStartInfo starter = new ProcessStartInfo(exe, arg);
            starter.WindowStyle = ProcessWindowStyle.Hidden;
            //starter.UseShellExecute = false;
            Process process = Process.Start(starter);
            process.WaitForExit();
        }
    }
}
