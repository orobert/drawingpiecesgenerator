﻿using CsPotrace;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace DrawingPiecesGenerator
{
    /*class SvgCreator
    {
        class SvgObj
        {
            public ArrayList Shapes;
            public Color Color;

            public SvgObj(ArrayList shapes, Color color)
            {
                this.Shapes = shapes;
                this.Color = color;
            }
        }

        private List<SvgObj> objects = new List<SvgObj>();

        public SvgCreator()
        {
        }

        public void add(ArrayList shapes)
        {
            add(shapes, Color.Black);
        }

        public void add(ArrayList shapes, Color color)
        {
            objects.Add(new SvgObj(shapes, color));
        }

        public void save(string path, float width, float height, bool forCut)
        {
            List<ArrayList> list = new List<ArrayList>();

            for (int i = 0; i < objects.Count; i++)
            {
                list.Add(objects[i].Shapes);
            }

            string xml = Potrace.Export2SVG(list, width, height);

            int pos = xml.IndexOf("<svg");
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(xml.Substring(pos));

            XmlElement root = xmlDoc.DocumentElement;
            root.SetAttribute("width", "304.79999mm");
            root.SetAttribute("height", "304.79999mm");
            root.SetAttribute("viewBox", "0 0 304.80001 304.80003");

            XmlNode svgNode = xmlDoc.ChildNodes[0];

            for (int i = 0; i < objects.Count; i++)
            {
                SvgObj obj = objects[i];

                XmlNode groupNode = svgNode.ChildNodes[i];
                {
                    XmlAttribute styleAttrib = xmlDoc.CreateAttribute("style");
                    if (forCut)
                        styleAttrib.Value = string.Format("fill:#{0:x6};stroke:none", (objects[i].Color.ToArgb() & 0xffffff));
                    else
                        styleAttrib.Value = string.Format("fill:none;stroke:#{0:x6};stroke-width:0.4", (objects[i].Color.ToArgb() & 0xffffff));
                    groupNode.Attributes.Append(styleAttrib);
                }
                {
                    XmlAttribute styleAttrib = xmlDoc.CreateAttribute("transform");
                    styleAttrib.Value = "translate(0,0)";
                    groupNode.Attributes.Append(styleAttrib);
                }
            }


            xml = xmlDoc.InnerXml;

            StreamWriter FS = new StreamWriter(path);
            FS.Write(xml);
            FS.Close();
        }
    }*/
}
