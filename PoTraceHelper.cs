﻿using CsPotrace;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingPiecesGenerator
{
    class PoTraceHelper
    {
        public static ArrayList cloneCurveArray(ArrayList sourceListCurveArray)
        {
            ArrayList destListCurveArray = new ArrayList();
            for (int k = 0; k < sourceListCurveArray.Count; k++)
            {
                ArrayList sourceCurveArray = (ArrayList)sourceListCurveArray[k];
                ArrayList destCurveArray = new ArrayList();
                destListCurveArray.Add(destCurveArray);

                for (int i = 0; i < sourceCurveArray.Count; i++)
                {
                    Potrace.Curve[] sourceCurves = (Potrace.Curve[])sourceCurveArray[i];
                    List<Potrace.Curve> destCurves = new List<Potrace.Curve>();

                    for (int j = 0; j < sourceCurves.Length; j++)
                    {
                        Potrace.Curve sourceCurve = sourceCurves[j];
                        Potrace.Curve destCurve = new Potrace.Curve();
                        destCurve.Kind = sourceCurve.Kind;
                        destCurve.A = sourceCurve.A;
                        destCurve.B = sourceCurve.B;
                        destCurve.ControlPointA = sourceCurve.ControlPointA;
                        destCurve.ControlPointB = sourceCurve.ControlPointB;
                        destCurves.Add(destCurve);
                    }

                    destCurveArray.Add(destCurves.ToArray());
                }
            }
            return destListCurveArray;
        }

        public static void offsetCurveArray(ref ArrayList listOfCurveArray, Point offsetPoint)
        {
            for (int k = 0; k < listOfCurveArray.Count; k++)
            {
                ArrayList path = (ArrayList)listOfCurveArray[k];

                for (int i = 0; i < path.Count; i++)
                {
                    Potrace.Curve[] curves = (Potrace.Curve[])path[i];

                    for (int j = 0; j < curves.Length; j++)
                    {
                        offsetCurvePoint(ref curves[j].A, offsetPoint);
                        offsetCurvePoint(ref curves[j].B, offsetPoint);
                        offsetCurvePoint(ref curves[j].ControlPointA, offsetPoint);
                        offsetCurvePoint(ref curves[j].ControlPointB, offsetPoint);
                    }
                }
            }
        }

        private static void offsetCurvePoint(ref Potrace.dPoint point, Point offsetPoint)
        {
            point.x += offsetPoint.X;
            point.y += offsetPoint.Y;
        }

        public static void scaleCurveArray(ref ArrayList listOfCurveArray, float scale)
        {
            for (int k = 0; k < listOfCurveArray.Count; k++)
            {
                ArrayList path = (ArrayList)listOfCurveArray[k];

                for (int i = 0; i < path.Count; i++)
                {
                    Potrace.Curve[] curves = (Potrace.Curve[])path[i];

                    for (int j = 0; j < curves.Length; j++)
                    {
                        scaleCurvePoint(ref curves[j].A, scale);
                        scaleCurvePoint(ref curves[j].B, scale);
                        scaleCurvePoint(ref curves[j].ControlPointA, scale);
                        scaleCurvePoint(ref curves[j].ControlPointB, scale);
                    }
                }
            }
        }

        private static void scaleCurvePoint(ref Potrace.dPoint point, float scale)
        {
            point.x *= scale;
            point.y *= scale;
        }

        public static ArrayList drawRectangle(RectangleF rect)
        {
            ArrayList shapes = new ArrayList();
            {
                Potrace.dPoint p1 = new Potrace.dPoint(rect.Left, rect.Top);
                Potrace.dPoint p2 = new Potrace.dPoint(rect.Right, rect.Top);
                Potrace.dPoint p3 = new Potrace.dPoint(rect.Right, rect.Bottom);
                Potrace.dPoint p4 = new Potrace.dPoint(rect.Left, rect.Bottom);

                Potrace.Curve[] curves = new Potrace.Curve[4];

                curves[0] = new Potrace.Curve(Potrace.CurveKind.Line, p1, p1, p2, p2);
                curves[1] = new Potrace.Curve(Potrace.CurveKind.Line, p2, p2, p3, p3);
                curves[2] = new Potrace.Curve(Potrace.CurveKind.Line, p3, p3, p4, p4);
                curves[3] = new Potrace.Curve(Potrace.CurveKind.Line, p4, p4, p1, p1);

                ArrayList shape = new ArrayList();
                shape.Add(curves);

                shapes.Add(shape);
            }
            return shapes;
        }

        public static ArrayList drawTriangle(PointF pA, PointF pB, PointF pC)
        {
            ArrayList shapes = new ArrayList();
            {
                Potrace.dPoint p1 = new Potrace.dPoint(pA.X, pA.Y);
                Potrace.dPoint p2 = new Potrace.dPoint(pB.X, pB.Y);
                Potrace.dPoint p3 = new Potrace.dPoint(pC.X, pC.Y);

                Potrace.Curve[] curves = new Potrace.Curve[3];

                curves[0] = new Potrace.Curve(Potrace.CurveKind.Line, p1, p1, p2, p2);
                curves[1] = new Potrace.Curve(Potrace.CurveKind.Line, p2, p2, p3, p3);
                curves[2] = new Potrace.Curve(Potrace.CurveKind.Line, p3, p3, p1, p1);

                ArrayList shape = new ArrayList();
                shape.Add(curves);

                shapes.Add(shape);
            }
            return shapes;
        }
    }
}
