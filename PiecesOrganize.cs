﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DrawingPiecesGenerator
{
    public class PiecesOrganize
    {
        const int CELL_SIZE = 30;

        const int NB_PAGE_CELLS_X = ImagesGenerator.PAGE_WIDTH / CELL_SIZE;
        const int NB_PAGE_CELLS_Y = ImagesGenerator.PAGE_HEIGHT / CELL_SIZE;

        public static void organize(string source, string page, List<Pieces.Piece> originalPieces)
        {
            DateTime srcDate = File.GetLastWriteTime(source);
            DateTime dataDate = DateTime.MinValue;

            string dir = Path.GetDirectoryName(source);
            string name = Path.GetFileNameWithoutExtension(source);

            string dataPath = Path.Combine(dir, "organize." + page + ".xml");

            if (File.Exists(dataPath))
            {
                dataDate = File.GetLastWriteTime(dataPath);
            }

            List<PieceOrganized> pieces;

            if (srcDate == dataDate)
            {
                pieces = loadPieces(dataPath);
            }
            else
            {
                pieces = new List<PieceOrganized>();

                int done = 0;
                foreach (Pieces.Piece piece in originalPieces)
                {
                    PieceOrganized pieceOrganized = new PieceOrganized();
                    pieceOrganized.Piece = piece;
                    pieceOrganized.PieceNo = piece.No;
                    pieceOrganized.Map = getMatrix(piece.IsolatedImage, out pieceOrganized.CellsCount);

                    pieces.Add(pieceOrganized);

                    done++;
                    Console.WriteLine("Piece #{0} prepared for page organization (To do: {1})", piece.No, originalPieces.Count - done);
                }

                pieces.Sort(new PieceOrganized.Sorter());

                arrangePieces(pieces);

                savePieces(dataPath, pieces);
                File.SetLastWriteTime(dataPath, srcDate);
            }

            foreach(PieceOrganized pieceOrganized in pieces)
            {
                Pieces.Piece piece = getPiece(originalPieces, pieceOrganized.PieceNo);

                piece.Page = pieceOrganized.Page;
                piece.PageImagePosition = pieceOrganized.PageImagePosition;
            }
        }

        private static Pieces.Piece getPiece(List<Pieces.Piece> pieces, int no)
        {
            foreach (Pieces.Piece piece in pieces)
            {
                if (piece.No == no)
                    return piece;
            }
            return null;
        }

        [Serializable]
        public class PieceOrganized
        {
            [NonSerialized]
            [XmlIgnore]
            public Pieces.Piece Piece;
            public int PieceNo;

            [NonSerialized]
            [XmlIgnore]
            public List<List<bool>> Map;
            [NonSerialized]
            [XmlIgnore]
            public int CellsCount;

            public int Page;
            public Point PageImagePosition;

            public class Sorter : IComparer<PieceOrganized>
            {
                public int Compare(PieceOrganized x, PieceOrganized y)
                {
                    return Comparer<int>.Default.Compare(y.CellsCount, x.CellsCount);
                }
            }
        }

        class Cell
        {
            public bool Filled = false;
        }

        class Page
        {
            public List<List<Cell>> Cells;
            public int No;

            public Page()
            {
                Cells = new List<List<Cell>>();

                for (int y = 0; y < NB_PAGE_CELLS_Y; y++)
                {
                    List<Cell> line = new List<Cell>();
                    Cells.Add(line);

                    for (int x = 0; x < NB_PAGE_CELLS_X; x++)
                    {
                        line.Add(null);
                    }
                }

                /*int nbCells = (ImagesGenerator.PAGE_MARKER_SIZE / CELL_SIZE) + 1;

                for (int y = 0; y < nbCells; y++)
                {
                    List<Cell> line = Cells[y];
                    for (int x = 0; x < nbCells; x++)
                    {
                        Cell cell = new Cell();
                        cell.Filled = true;
                        line[x] = cell;
                    }
                }*/
            }
        }

        private static List<List<bool>> getMatrix(Bitmap source, out int count)
        {
            Bitmap bmp = new Bitmap(source);
            ImageProcessing.invert(bmp);

            bmp = ImageProcessing.addShapeBorder(bmp, 20);

            count = 0;
            int w = bmp.Width, h = bmp.Height;
            BitmapData data = bmp.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);

            int[] bits = new int[data.Stride / 4 * data.Height];
            Marshal.Copy(data.Scan0, bits, 0, bits.Length);

            List<List<bool>> cells = new List<List<bool>>();
            for (int y = 0; y < h; y += CELL_SIZE)
            {
                List<bool> cellsX = new List<bool>();
                cells.Add(cellsX);
                for (int x = 0; x < w; x += CELL_SIZE)
                {
                    bool haveWhite = false;
                    for (int j = 0; j < CELL_SIZE; j++)
                    {
                        for (int i = 0; i < CELL_SIZE; i++)
                        {
                            if (((x + i) < w) && ((y + j) < h))
                            {
                                int c = bits[(x + i) + (y + j) * data.Stride / 4];
                                if ((c & 0xffffff) == 0xffffff)
                                {
                                    haveWhite = true;
                                    count++;
                                    break;
                                }
                            }
                        }
                        if (haveWhite)
                            break;
                    }

                    cellsX.Add(haveWhite);
                }

            }

            bmp.UnlockBits(data);

            return cells;
        }

        static void arrangePieces(List<PieceOrganized> pieces)
        {
            List<Page> pages = new List<Page>();
            Page p = new Page();
            p.No = 1;
            pages.Add(p);

            int done = 0;
            foreach (PieceOrganized piece in pieces)
            {
                bool found = false;
                while (!found)
                {
                    found = foundPieceLocation(pages, piece);
                    if (!found)
                    {
                        p = new Page();
                        p.No = pages.Count + 1;
                        pages.Add(p);
                    }
                }

                done++;
                Console.WriteLine("{0} pieces to place in {1} page(s)", pieces.Count - done, pages.Count);
            }

            //applyArrangement(pieces);
        }

        private static bool foundPieceLocation(List<Page> pages, PieceOrganized piece)
        {
            for (int p = 0; p < pages.Count; p++)
            {
                Page page = pages[p];
                for (int y = 0; y < page.Cells.Count; y++)
                {
                    List<Cell> line = page.Cells[y];
                    for (int x = 0; x < line.Count; x++)
                    {
                        if (isPieceCanFit(page.Cells, piece, x, y))
                        {
                            placePieceInCells(page, piece, x, y);
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        private static bool isPieceCanFit(List<List<Cell>> cells, PieceOrganized piece, int x, int y)
        {
            int matrixCountX = piece.Map[0].Count;
            int matrixCountY = piece.Map.Count;

            int matrixY = 0;
            for (int j = y; j < y + matrixCountY; j++)
            {
                if (j >= cells.Count)
                    return false;

                List<Cell> line = cells[j];
                List<bool> lineMatrix = piece.Map[matrixY];
                int matrixX = 0;
                for (int i = x; i < x + matrixCountX; i++)
                {
                    if (i >= line.Count)
                        return false;

                    if (lineMatrix[matrixX] && line[i] != null && (line[i].Filled))
                        return false;

                    matrixX++;
                }

                matrixY++;
            }

            return true;
        }

        private static void placePieceInCells(Page page, PieceOrganized piece, int x, int y)
        {
            int matrixCountX = piece.Map[0].Count;
            int matrixCountY = piece.Map.Count;

            for (int j = y; j < y + matrixCountY; j++)
            {
                List<Cell> line = page.Cells[j];
                for (int i = x; i < x + matrixCountX; i++)
                {
                    placePieceInCell(piece, line, x, y, i, j);
                }
            }

            piece.Page = page.No;
            piece.PageImagePosition = new Point((x * CELL_SIZE), (y * CELL_SIZE));
        }

        private static void placePieceInCell(PieceOrganized piece, List<Cell> line, int x, int y, int i, int j)
        {
            Cell cell = new Cell();

            cell.Filled = piece.Map[j - y][i - x];
            if (i < line.Count)
                if (line[i] != null)
                {
                    if (cell.Filled && !line[i].Filled)
                        line[i].Filled = true;
                }
                else
                {
                    line[i] = cell;
                }
        }

        //static void applyArrangement(List<PieceOrganized> pieces)
        //{
        //    //List<Bitmap> bmps = new List<Bitmap>();
        //    //List<Graphics> grs = new List<Graphics>();

        //    //for (int p = 0; p < pages.Count; p++)
        //    //{
        //    //    Page page = pages[p];

        //    //    Bitmap bmp = new Bitmap(PAGE_WIDTH, PAGE_HEIGHT);
        //    //    Graphics gr = Graphics.FromImage(bmp);
        //    //    gr.Clear(Color.Black);

        //    //    for (int y = 0; y < page.Cells.Count; y++)
        //    //    {
        //    //        List<Cell> line = page.Cells[y];
        //    //        for (int x = 0; x < line.Count; x++)
        //    //        {
        //    //            Cell cell = line[x];

        //    //            if (cell != null)
        //    //            {
        //    //                Brush br;
        //    //                if (cell.Filled)
        //    //                    br = new SolidBrush(Color.FromArgb(128, 0, 0));
        //    //                else
        //    //                    br = new SolidBrush(Color.FromArgb(64, 0, 0));

        //    //                Rectangle rect = new Rectangle(x * CELL_SIZE, y * CELL_SIZE, CELL_SIZE - 2, CELL_SIZE - 2);
        //    //                gr.FillRectangle(br, rect);
        //    //            }
        //    //        }
        //    //    }

        //    //    bmps.Add(bmp);
        //    //    grs.Add(gr);
        //    //}

        //    foreach(PieceOrganized piece in pieces)
        //    {
        //        //piece.PageImagePosition = new Point((piece.CellPosition.X * CELL_SIZE), (piece.CellPosition.Y * CELL_SIZE));

        //        //// copy isolated to page bitmap
        //        //Bitmap pMap = new Bitmap(piece.Piece.IsolatedImage);
        //        //ImageProcessing.invert(pMap);
        //        //pMap.MakeTransparent(Color.Black);

        //        //Rectangle rectSrc = new Rectangle(new Point(0, 0), pMap.Size);
        //        //RectangleF rectDest = new Rectangle(piece.PageImagePosition, pMap.Size);
        //        //grs[piece.Page - 1].DrawImage(pMap, rectDest, rectSrc, GraphicsUnit.Pixel);
        //    }

        //    //for (int p = 0; p < pages.Count; p++)
        //    //{
        //    //    Page page = pages[p];
        //    //    bmps[p].Save(@"D:\vitrail\test1\drawing_files\page" + page.No.ToString() + ".png");
        //    //}
        //}

        private static void savePieces(string path, List<PieceOrganized> pieces)
        {
            string dir = Path.GetDirectoryName(path);
            string name = Path.GetFileNameWithoutExtension(path);
            string piecesDir = Path.Combine(dir, name);

            XmlSerializer xml = new XmlSerializer(typeof(List<PieceOrganized>));
            FileStream file = new FileStream(path, FileMode.Create);

            xml.Serialize(file, pieces);

            file.Flush();
            file.Close();
        }
        
        private static List<PieceOrganized> loadPieces(string path)
        {
            string dir = Path.GetDirectoryName(path);
            string name = Path.GetFileNameWithoutExtension(path);
            string piecesDir = Path.Combine(dir, name);

            XmlSerializer xml = new XmlSerializer(typeof(List<PieceOrganized>));
            FileStream file = new FileStream(path, FileMode.Open);

            List<PieceOrganized> pieces = (List<PieceOrganized>)xml.Deserialize(file);

            file.Close();

            return pieces;
        }
    }
}
