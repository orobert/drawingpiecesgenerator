﻿using ImageMagick;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace DrawingPiecesGenerator
{
    class ImageProcessing
    {
        public static Bitmap normalizeImage(Bitmap baseImage)
        {
            Bitmap image = new Bitmap(baseImage);

            BitmapData dataSource = baseImage.LockBits(
                new Rectangle(0, 0, baseImage.Width, baseImage.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            int[] bitsSource = new int[dataSource.Stride / 4 * dataSource.Height];
            Marshal.Copy(dataSource.Scan0, bitsSource, 0, bitsSource.Length);

            BitmapData dataDest = image.LockBits(
                new Rectangle(0, 0, image.Width, image.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            int[] bitsDest = new int[dataDest.Stride / 4 * dataDest.Height];
            Marshal.Copy(dataDest.Scan0, bitsDest, 0, bitsDest.Length);

            int black = Color.Black.ToArgb();
            int white = Color.White.ToArgb();

            int h = baseImage.Height;
            int w = baseImage.Width;

            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    int c = bitsSource[x + y * dataSource.Stride / 4];
                    int i = (x + y * dataDest.Stride / 4);
                    if ((c & 0xffffff) == 0)
                    {
                        bitsDest[i] = black;
                    }
                    else
                    {
                        bitsDest[i] = white;
                    }
                }
            }

            Marshal.Copy(bitsDest, 0, dataDest.Scan0, bitsDest.Length);

            image.UnlockBits(dataSource);
            baseImage.UnlockBits(dataDest);

            return image;
        }

        public static void floodFill(Bitmap bitmap, int x, int y, Color color)
        {
            BitmapData data = bitmap.LockBits(
                new Rectangle(0, 0, bitmap.Width, bitmap.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            int[] bits = new int[data.Stride / 4 * data.Height];
            Marshal.Copy(data.Scan0, bits, 0, bits.Length);

            LinkedList<Point> check = new LinkedList<Point>();
            int floodTo = color.ToArgb();
            int floodFrom = bits[x + y * data.Stride / 4];
            bits[x + y * data.Stride / 4] = floodTo;

            if (floodFrom != floodTo)
            {
                check.AddLast(new Point(x, y));
                while (check.Count > 0)
                {
                    Point cur = check.First.Value;
                    check.RemoveFirst();

                    foreach (Point off in new Point[] {
                new Point(0, -1), new Point(0, 1), 
                new Point(-1, 0), new Point(1, 0)})
                    {
                        Point next = new Point(cur.X + off.X, cur.Y + off.Y);
                        if (next.X >= 0 && next.Y >= 0 &&
                            next.X < data.Width &&
                            next.Y < data.Height)
                        {
                            if (bits[next.X + next.Y * data.Stride / 4] == floodFrom)
                            {
                                check.AddLast(next);
                                bits[next.X + next.Y * data.Stride / 4] = floodTo;
                            }
                        }
                    }
                }
            }

            Marshal.Copy(bits, 0, data.Scan0, bits.Length);
            bitmap.UnlockBits(data);
        }

        public static Bitmap isolateImage(Bitmap source, Color usedColor, out Point imagePoint)
        {
            BitmapData dataSource = source.LockBits(
                new Rectangle(0, 0, source.Width, source.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            int bitsCount = dataSource.Stride / 4 * dataSource.Height;
            int[] bits = new int[bitsCount];
            Marshal.Copy(dataSource.Scan0, bits, 0, bits.Length);

            int h = source.Height;
            int w = source.Width;

            int color = usedColor.ToArgb();

            int minY = h;
            int maxY = 0;
            int minX = w;
            int maxX = 0;
            int count = 0;
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    int c = bits[x + y * dataSource.Stride / 4];

                    if (c == color)
                    {
                        if (y < minY)
                            minY = y;
                        if (y > maxY)
                            maxY = y;
                        if (x < minX)
                            minX = x;
                        if (x > maxX)
                            maxX = x;
                        count++;
                    }
                }
            }
            source.UnlockBits(dataSource);

            if (count < 20)
            {
                imagePoint = new Point();
                return null;
            }

            Rectangle zone = new Rectangle(minX, minY, maxX - minX + 1, maxY - minY + 1);

            imagePoint = new Point(minX - 10, minY - 10);

            Bitmap image = new Bitmap(zone.Width + 20, zone.Height + 20);
            Graphics gr = Graphics.FromImage(image);
            gr.Clear(Color.Black);

            transfertImage(source, Color.Red, image, Color.White, zone, new Point(10, 10));

            return image;
        }

        public static void transfertImage(Bitmap srcImage, Color srcColor, Bitmap destImage, Color destColor, Rectangle zone, Point point)
        {
            BitmapData dataSource = srcImage.LockBits(
                new Rectangle(0, 0, srcImage.Width, srcImage.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            int[] bitsSource = new int[dataSource.Stride / 4 * dataSource.Height];
            Marshal.Copy(dataSource.Scan0, bitsSource, 0, bitsSource.Length);

            BitmapData dataDest = destImage.LockBits(
                new Rectangle(0, 0, destImage.Width, destImage.Height),
                ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);
            int[] bitsDest = new int[dataDest.Stride / 4 * dataDest.Height];
            Marshal.Copy(dataDest.Scan0, bitsDest, 0, bitsDest.Length);


            int srcC = srcColor.ToArgb();
            int destC = destColor.ToArgb();

            for (int y = zone.Top; y < zone.Bottom; y++)
            {
                for (int x = zone.Left; x < zone.Right; x++)
                {
                    int c = bitsSource[x + y * dataSource.Stride / 4];

                    if (c == srcC)
                    {
                        bitsDest[(x - zone.X + point.X) + (y - zone.Y + point.Y) * dataDest.Stride / 4] = destC;
                    }
                }
            }

            Marshal.Copy(bitsDest, 0, dataDest.Scan0, bitsDest.Length);

            srcImage.UnlockBits(dataSource);
            destImage.UnlockBits(dataDest);
        }

        public static void invert(Bitmap bmp)
        {
            int w = bmp.Width, h = bmp.Height;
            BitmapData data = bmp.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.ReadWrite, PixelFormat.Format32bppArgb);

            int[] bits = new int[data.Stride / 4 * data.Height];
            Marshal.Copy(data.Scan0, bits, 0, bits.Length);

            for (int i = w * h - 1; i >= 0; i--)
                bits[i] = (~bits[i]) + ~0xffffff;

            Marshal.Copy(bits, 0, data.Scan0, bits.Length);

            bmp.UnlockBits(data);
        }

        public static Bitmap addShapeBorder(Bitmap baseImage, int size)
        {
            MemoryStream stream = new MemoryStream();

            using (MagickImage image = new MagickImage(baseImage))
            {
                MorphologyMethod method = MorphologyMethod.Dilate;
                if (size < 0)
                {
                    method = MorphologyMethod.Erode;
                    size = -size;
                }
                image.Morphology(method, Kernel.Square, size);

                image.Write(stream, MagickFormat.Png);
                stream.Seek(0, SeekOrigin.Begin);
            }

            Bitmap borderImage = new Bitmap(stream);
            return borderImage;
        }
    }
}
