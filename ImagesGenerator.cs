﻿using CsPotrace;
using SvgDrawing;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawingPiecesGenerator
{
    class ImagesGenerator
    {
        public const int FULL_DPI = 450;
        public const int PRINT_DPI = 90;

        //public const float PAGE_INCES_WIDTH = 6;
        //public const float PAGE_INCES_HEIGHT = 8.5f;
        public const float PAGE_INCES_WIDTH = 8.5f - (2 * 0.25f);
        public const float PAGE_INCES_HEIGHT = 11.0f - (2 * 0.25f);

        public const int PAGE_WIDTH = (int)(PAGE_INCES_WIDTH * FULL_DPI);
        public const int PAGE_HEIGHT = (int)(PAGE_INCES_HEIGHT * FULL_DPI);

        public const float MARKER_SIZE = 0.5f;
        public const int PAGE_MARKER_SIZE = (int)(MARKER_SIZE * FULL_DPI);


        public static void generateImages(Pieces.ExtractionResult result, string sourcePath, string name)
        {
            string path = Path.Combine(sourcePath, name);

            if (Directory.Exists(path))
            {
                foreach (string file in Directory.GetFiles(path, "*.*", SearchOption.TopDirectoryOnly))
                {
                    try
                    {
                        File.Delete(file);
                    }
                    catch { }
                }
            }
            else
            {
                Directory.CreateDirectory(path);
            }

            generateMasterPicture(result, Path.Combine(path, "master.png"));

            Dictionary<int, List<Pieces.Piece>> pages = sortByPages(result);

            List<SvgDocument> svgPages = new List<SvgDocument>();
            foreach (KeyValuePair<int, List<Pieces.Piece>> item in pages)
            {
                svgPages.Add(new SvgDocument());
            }

            generateCutPages(pages, svgPages);
            generateWritePages(pages, svgPages);

            for (int i = 0; i < svgPages.Count; i++)
            {
                SvgDocument svgPage = svgPages[i];
                svgPage.Save(Path.Combine(path, string.Format("{0} page {1}.svg", name, i + 1)));
            }
        }

        /*private static void DeleteFolder(string path, bool top)
        {
            foreach (string folder in Directory.GetDirectories(path, "*.*", SearchOption.TopDirectoryOnly))
            {
                DeleteFolder(folder, false);
            }

            foreach (string file in Directory.GetFiles(path, "*.*", SearchOption.TopDirectoryOnly))
            {
                try
                {
                    File.Delete(file);
                }
                catch { }
            }

            if (!top)
            {
                try
                {
                    Directory.Delete(path);
                }
                catch { }
            }
        }*/

        private static void generateMasterPicture(Pieces.ExtractionResult result, string path)
        {
            foreach (Pieces.Piece piece in result.Pieces)
            {
                writePieceInfo(result.MasterImage, piece, Color.Black, piece.CenterPoint);
            }

            result.MasterImage.SetResolution(FULL_DPI, FULL_DPI);
            result.MasterImage.Save(path);
        }

        private static Dictionary<int, List<Pieces.Piece>> sortByPages(Pieces.ExtractionResult result)
        {
            Dictionary<int, List<Pieces.Piece>> list = new Dictionary<int, List<Pieces.Piece>>();

            foreach (Pieces.Piece piece in result.Pieces)
            {
                List<Pieces.Piece> pieces;
                if (!list.TryGetValue(piece.Page, out pieces))
                {
                    pieces = new List<Pieces.Piece>();
                    list.Add(piece.Page, pieces);
                }
                pieces.Add(piece);
            }

            return list;
        }

        private static void generateCutPages(Dictionary<int, List<Pieces.Piece>> pages, List<SvgDocument> svgPages)
        {
            float wF = (int)(PAGE_INCES_WIDTH * PRINT_DPI);
            float hF = (int)(PAGE_INCES_HEIGHT * PRINT_DPI);

            float scale = (float)PRINT_DPI / FULL_DPI;

            float scaleMM = 1 / 3.543307087f;

            int i = 0;
            foreach (KeyValuePair<int, List<Pieces.Piece>> page in pages)
            {
                SvgDocument svgPage = svgPages[page.Key - 1];

                //ArrayList border = PoTraceHelper.drawRectangle(new RectangleF(0, 0, wF, hF));
                //PoTraceHelper.scaleCurveArray(ref border, scaleMM);
                //svgCreator.add(border, Color.Turquoise);
                /*{
                    float w = (int)(MARKER_SIZE * PRINT_DPI);
                    float h = (int)(MARKER_SIZE * PRINT_DPI);

                    ArrayList border = PoTraceHelper.drawTriangle(new PointF(w, 0), new PointF(0, h), new PointF(0, 0));// new RectangleF(0, 0, wF, hF));
                    PoTraceHelper.scaleCurveArray(ref border, scaleMM);
                    svgPage.AddCutShape(new SvgShape(border, Color.Black));
                }*/

                ArrayList pieces = new ArrayList();
                //*
                foreach (Pieces.Piece piece in page.Value)
                {
                    ArrayList pieceShape = PoTraceHelper.cloneCurveArray(piece.IsolatedImageShape);
                    PoTraceHelper.offsetCurveArray(ref pieceShape, piece.PageImagePosition);
                    PoTraceHelper.scaleCurveArray(ref pieceShape, scale);
                    PoTraceHelper.scaleCurveArray(ref pieceShape, scaleMM);

                    pieces.AddRange(pieceShape);
                }
                /*/
                foreach (Pieces.Piece piece in page.Value)
                {
                    Bitmap map = new Bitmap(piece.IsolatedImage);
                    ImageProcessing.invert(map);
                    //map = ImageProcessing.addShapeBorder(map, -3);
                    ImageProcessing.invert(map);

                    bool[,] matrix = Potrace.BitMapToBinary(map, 130);
                    ArrayList textShape = new ArrayList();
                    Potrace.potrace_trace(matrix, textShape);

                    PoTraceHelper.offsetCurveArray(ref textShape, piece.PageImagePosition);
                    PoTraceHelper.scaleCurveArray(ref textShape, scale);
                    PoTraceHelper.scaleCurveArray(ref textShape, scaleMM);

                    pieces.AddRange(textShape);
                }
                //*/
                svgPage.AddCutShape(new SvgShape(pieces, Color.Black));


                i++;
                Console.WriteLine("Cut page #{0} written", i);
            }
        }

        private static void generateWritePages(Dictionary<int, List<Pieces.Piece>> pages, List<SvgDocument> svgPages)
        {
            float wF = (int)(PAGE_INCES_WIDTH * PRINT_DPI);
            float hF = (int)(PAGE_INCES_HEIGHT * PRINT_DPI);

            float scale = (float)PRINT_DPI / FULL_DPI;

            float scaleMM = 1 / 3.543307087f;

            int i = 0;
            foreach (KeyValuePair<int, List<Pieces.Piece>> page in pages)
            {
                SvgDocument svgPage = svgPages[page.Key - 1];

                //ArrayList border = PoTraceHelper.drawRectangle(new RectangleF(0, 0, wF, hF));
                //svgCreator.add(border, Color.Turquoise);
                /*{
                    float w = (int)(MARKER_SIZE * PRINT_DPI);
                    float h = (int)(MARKER_SIZE * PRINT_DPI);

                    ArrayList border = PoTraceHelper.drawTriangle(new PointF(w, 0), new PointF(0, h), new PointF(0, 0));// new RectangleF(0, 0, wF, hF));
                    PoTraceHelper.scaleCurveArray(ref border, scaleMM);
                    svgCreator.add(border, Color.Black);
                }*/

                ArrayList pieces = new ArrayList();
                foreach (Pieces.Piece piece in page.Value)
                {
                    ArrayList pieceShape = PoTraceHelper.cloneCurveArray(piece.IsolatedImageShape);
                    PoTraceHelper.offsetCurveArray(ref pieceShape, piece.PageImagePosition);
                    PoTraceHelper.scaleCurveArray(ref pieceShape, scale);
                    PoTraceHelper.scaleCurveArray(ref pieceShape, scaleMM);

                    pieces.AddRange(pieceShape);
                }
                svgPage.AddWriteShape(new SvgShape(pieces, Color.Black));

                /*pieces = new ArrayList();
                foreach (Pieces.Piece piece in page.Value)
                {
                    Bitmap map = new Bitmap(piece.IsolatedImage);
                    ImageProcessing.invert(map);
                    map = ImageProcessing.addShapeBorder(map, -3);
                    ImageProcessing.invert(map);

                    bool[,] matrix = Potrace.BitMapToBinary(map, 130);
                    ArrayList pieceShape = new ArrayList();
                    Potrace.potrace_trace(matrix, pieceShape);

                    PoTraceHelper.offsetCurveArray(ref pieceShape, piece.PageImagePosition);
                    PoTraceHelper.scaleCurveArray(ref pieceShape, scale);
                    PoTraceHelper.scaleCurveArray(ref pieceShape, scaleMM);

                    pieces.AddRange(pieceShape);
                }
                svgPage.AddWriteShape(new SvgShape(pieces, Color.Black));*/

                ArrayList texts = new ArrayList();
                foreach (Pieces.Piece piece in page.Value)
                {
                    Bitmap map = new Bitmap(piece.IsolatedImage.Width, piece.IsolatedImage.Height);
                    Graphics gr = Graphics.FromImage(map);
                    gr.Clear(Color.White);

                    writePieceInfo(map, piece, Color.Black, piece.IsolatedImageCenterPoint);

                    bool[,] matrix = Potrace.BitMapToBinary(map, 130);
                    ArrayList textShape = new ArrayList();
                    Potrace.potrace_trace(matrix, textShape);

                    PoTraceHelper.offsetCurveArray(ref textShape, piece.PageImagePosition);
                    PoTraceHelper.scaleCurveArray(ref textShape, scale);
                    PoTraceHelper.scaleCurveArray(ref textShape, scaleMM);

                    texts.AddRange(textShape);
                }
                svgPage.AddWriteShape(new SvgShape(texts, Color.Black));

                i++;
                Console.WriteLine("Write page #{0} written", i);
            }
        }

        private static void writePieceInfo(Bitmap image, Pieces.Piece piece, Color color, Point centerPoint)
        {
            Graphics gr = Graphics.FromImage(image);

            string drawString1 = piece.No.ToString();
            String drawString2 = piece.ColorLegend;

            Font drawFont1 = new Font("Arial", 72);
            Font drawFont2 = new Font("Arial", 54);

            SizeF string1Size = gr.MeasureString(drawString1, drawFont1);
            SizeF string2Size = gr.MeasureString(drawString2, drawFont2);

            float height1 = string1Size.Height - 25;
            float height2 = string2Size.Height;

            PointF drawPoint1 = new PointF(centerPoint.X - (string1Size.Width / 2), centerPoint.Y - ((height1 + height2) / 2));
            PointF drawPoint2 = new PointF(centerPoint.X - (string2Size.Width / 2), drawPoint1.Y + height1);

            Brush br = new SolidBrush(color);
            gr.DrawString(drawString1, drawFont1, br, drawPoint1);
            gr.DrawString(drawString2, drawFont2, br, drawPoint2);
        }

    }
}
